# Registrar

A simple dumb registrar to keep track of agents that advertise themselves, with an authentication token.

Agents that need to register with a central node can run curl on a timer to update regularly with the appropriate call.

The `client_query` tool can be used to query the registrar, accessing using a token.

## Why

Troubleshooting/palliative tool mainly. Some people assign IPs to devices blindly and cause IP clashes. Some devices change IP and cannot update DNS. This tool is a workaround for similar scenarios.
