__TOKENS = None

def validate_token(agent_name, agent_token):
    global __TOKENS

    ensure_tokens_loaded("tokens.txt")

    return ( __TOKENS.get(agent_name, None) == agent_token )

def ensure_tokens_loaded(tokens_file):
    global __TOKENS
    token_lines = None

    if __TOKENS:
        return

    with open(tokens_file, 'r') as fh:
        token_lines = fh.readlines()

    for line in token_lines:
        k,v = line.split("=",1)
        __TOKENS[k.strip()] = v.strip()
