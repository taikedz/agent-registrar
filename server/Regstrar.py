import http.server

import TokenValidator
import IpStore

class RegistrarHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        self.parseQuery()

        agent_name = self.query.get("agent", None)
        agent_ip = self.query.get("ip", None)
        agent_token = self.query.get("token", None)

        if TokenValidator.validate_token(agent_name, agent_token):
            IpStore.register_IP(agent_name, agent_ip)
            self.respondWith("Registered.")
        else:
            self.respondWith("Invalid token.", code=401)


    def respondWith(self, data, mime="text/plain", code=200):
        self.send_response(code)
        self.send_header("Content-type", mime)
        self.end_headers()

        self.wfile.write(bytes(data, 'utf-8') )


    def parseQuery(self):
        if not hasattr(self, "query"):
            setattr(self, "query", {})
        else:
            return

        q_string = None
        q_string_idx = self.path.find("?")

        if q_string_idx < 0:
            return

        q_string = self.path[q_string_idx+1:]
        q_list = q_string.split("&")

        for item in q_list:
            q_key,q_val = item.split("=", 1)
            self.query[q_key] = q_val


def run(server_class=http.server.HTTPServer, handler_class=RegistrarHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

run()
